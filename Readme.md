# QR Code Creator for ICAL Events

This service offers a REST endpoint to create a QR code to import some event into a calendar.

## Run
```
docker build -t qr .
docker run -p "8082:8082" qr
```
## Environment
Variable | Meaning | Default
--- | --- | ---
PORT | The port this service proxy will run on | 8082
SERVICE_URL | public accessible root path for this service | http://localhost:8082

## Usage

Send HTTP Post

```
    POST /code HTTP/1.1
    Content-Type: aplication/json; charset=utf-8
    Host: localhost:8082
    Connection: close
    Content-Length: 210

    {
	    "Summary":"Sample Event",
	    "Location":"Madison Street",
	    "URL":"https://example.com",
	    "Start":"20190920T120000",
	    "End":"20190920T160000",
	    "Description":"Description of the event",
	    "AddAlertBefore":"1D"
    }
 ```

## Attention!
`Start` and `End` MUST match the format, otherwise ICAL clients will not import the event correct. This service does not perform validation!

`AddAlertBefore` will accept strings like "1D" (one day before the event) or "30M" (30 minutes before the event). Case matters!
If missing or left empty no alert will be created, and returned QR code will be "smaller".
## Result
If successful, the server will send the generated QR-Code in the response

![Sample png](sample.png)


## UI
The service ships with an (ugly) default template. The template can be replaced by mounting a volume `/html` that contains a single index.html
The path to the service itself must be replaced with `{{.}}`
This application will then replace this with the SERVICE_URL environment variable.
If additional static files, like javascript or css are required, the mount them into the `/static` volume.
In the index.html, they can be accessed using something like `{{.}}/static/myCustomFile.js`