package main

import (
	"encoding/json"
	"fmt"
	"github.com/skip2/go-qrcode"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"
)

var alert = `
BEGIN:VALARM
TRIGGER:-P%s
ACTION:DISPLAY
DESCRIPTION: %s
END:VALARM
`

var calendar = `
BEGIN:VCALENDAR
BEGIN:VEVENT
SUMMARY:%s
LOCATION:%s
URL:%s
DTSTART:%s
DTEND:%s
DESCRIPTION:%s
%s
END:VEVENT
END:VCALENDAR
`
var tpl = template.Must(template.ParseFiles("/html/index.html"))

const ICAL_TIMESTAMP_LAYOUT = "20060102T150405"

// StatusError represents an error with an associated HTTP status code.
type StatusError struct {
	Code int
	Err  error
}

type QRCodeRequest struct {
	Summary     string
	Location    string
	URL         string
	Start       time.Time
	End         time.Time
	Description string
	diff        int
	unit        byte
	createAlert bool
}

func main() {
	// All those requests will be mapped to index.html, where the SERVICE_URL is getting injected
	http.HandleFunc("/", index)

	// Endpoint accepting POST to create a QR code
	http.HandleFunc("/code", qrCodeEndpoint)

	// Static content, if required
	fs := http.FileServer(http.Dir("/static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}

func qrCodeEndpoint(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	//Deserialize request
	decoder := json.NewDecoder(r.Body)
	var t QRCodeRequest
	err := decoder.Decode(&t)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	png := createQrCode(fmt.Sprintf(calendar, t.Summary, t.Location, t.URL, icalTime(t.Start), icalTime(t.End), t.Description, t.getAlertString()))
	w.Header().Add("Content-type", "image/png")
	_, _ = w.Write(png)
}

func icalTime(time time.Time) string {
	return time.Format(ICAL_TIMESTAMP_LAYOUT)
}

func index(w http.ResponseWriter, r *http.Request) {
	_ = tpl.Execute(w, os.Getenv("SERVICE_URL"))
}

func (r *QRCodeRequest) getAlertString() string {
	if !r.createAlert {
		return ""
	}
	triggerSuffix := string(r.diff) + string(r.unit)
	return fmt.Sprintf(alert, triggerSuffix, r.Description)
}

func createQrCode(input string) []byte {
	png, err := qrcode.Encode(input, qrcode.Medium, 256)
	if err != nil {
		log.Fatal(err)
	}
	return png
}
