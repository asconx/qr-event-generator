FROM golang:1.8.5-jessie as builder
RUN go get github.com/skip2/go-qrcode

WORKDIR /go/src/app
ADD main.go main.go
# build the source
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main main.go

# use a minimal alpine image
FROM alpine:3.7
# add ca-certificates in case you need them
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
# set working directory
WORKDIR /root

ENV PORT 8082
ENV SERVICE_URL http://localhost:8082

# copy the binary from builder
COPY --from=builder /go/src/app/main .
COPY html /html
COPY static /static
# run the binary
CMD ["./main"]